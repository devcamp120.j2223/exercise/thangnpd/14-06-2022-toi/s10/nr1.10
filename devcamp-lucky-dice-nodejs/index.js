const express = require('express');
const app = express();
const port = 8000;

//Import middleware
const luckydiceMiddleware = require('./app/middlewares/luckydiceMiddleware')

app.use('/', luckydiceMiddleware);

//Return random number from 1 to 6 
app.get('/random-number', (req, res) => {
  let randomNum = Math.floor(Math.random() * (6 - 1 + 1)) + 1;
  res.json({
    randomNum: randomNum
  });
})

app.listen(port, () => {
  console.log(`NR1.10 app listening on port ${port}`);
})